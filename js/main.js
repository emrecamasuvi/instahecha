// ready go
// jquery start
$(function(){

  var footer_instagram = $("#footer_instagram_helper");
  var footer_instagram_tooltip = footer_instagram.find(".insta-tooltip");
  footer_instagram.hoverIntent({
    over: show_instagram_tooltip,
    out: hide_instagram_tooltip,
    selector: ''
  });

  function show_instagram_tooltip(){
    footer_instagram_tooltip.show();
  }
  function hide_instagram_tooltip(){
    footer_instagram_tooltip.hide();
  }

  $("#load-more-pics").on("click", ".block-link", function(e){
    e.preventDefault();
    // ajax load more pics
    alert("LOADED");
  });

  // flexslider for slider
  if ($.flexslider){
    $('.flexslider').flexslider({
      animation: "slide",
      controlNav: false,
      prevText: "&larr;",
      nextText: "&rarr;",
    });
  };

  // jquery validation plugin
  // further options at http://docs.jquery.com/Plugins/Validation
  if ($.validator){
    $("#application_form").validate();
  }


});